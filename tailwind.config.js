module.exports = {
  theme: {},
  variants: {},
  plugins: [],
  purge: {
    options: {
      safelist: ['bg-green-400', 'bg-blue-500', 'bg-red-400', 'bg-yellow-300']
    },
    content: [
      `components/**/*.{vue,js}`,
      `layouts/**/*.vue`,
      `store/**/*.js`,
      `pages/**/*.vue`,
      `plugins/**/*.{js,ts}`,
      `nuxt.config.{js,ts}`
    ]
  }
}

