# Parchis

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

### Working

- Only one player
- Half of the basic rules
- Interactive and automatic

### TODO

- Improve UI/UX
- Implement more players
- All the basic rules
- Tesing
- Refactoring and code cleanup
