class Shape {
    constructor( xPosition, yPosition, stroke=null, fill=null, strokeWidth=null){
        this.x = xPosition
        this.y = yPosition
        this.stroke = stroke
        this.fill = fill
        this.strokeWidth = strokeWidth
       
    }

}

class Rectangle extends Shape {
            constructor({xPosition, yPosition, height, width, color, strokeColor=null, strokeWidth=null}){
                    super(xPosition, yPosition, strokeColor, strokeWidth)
                    this.height = height;
                    this.width = width;
                    this.fill = color;
                    this.shape = 'rectangle'
            }
}

class trapezium extends Shape {
        constructor({xPosition, yPosition, offX, offY, color, strokeColor=null, strokeWidth}){
            super(xPosition, yPosition, color, strokeColor, this.stroke)
            this.shape = 'trapezium'
            this.offx = offX,
            this.offy = offY
        }

}

export {
    Shape,
    Rectangle
}